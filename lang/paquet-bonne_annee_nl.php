<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-bonne_annee?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bonne_annee_description' => 'Toon in de maand januari van ieder jaar een nieuwjaarswens.
Voeg <code>#INCLURE{fond=bonne_annee}</code> toe aan je bladzijdes.',
	'bonne_annee_nom' => 'Nieuwjaarswens',
	'bonne_annee_slogan' => 'Nieuwjaarswensen op je site'
);
