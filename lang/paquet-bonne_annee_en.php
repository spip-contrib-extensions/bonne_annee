<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-bonne_annee?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bonne_annee_description' => 'Displays every year during the month of January, a message wishing a happy new year.
Add <code>#INCLURE{fond=bonne_annee}</code> in your pages.',
	'bonne_annee_nom' => 'Happy new year',
	'bonne_annee_slogan' => 'Wish a happy new year on your site'
);
