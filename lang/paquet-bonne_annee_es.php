<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-bonne_annee?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bonne_annee_description' => 'Cada año durante el mes de Enero, muestra un mensaje de felicitación para el año nuevo.
Añadir <code>#INCLURE{fond=bonne_annee}</code> en su paginas.',
	'bonne_annee_nom' => 'Feliz año nuevo'
);
