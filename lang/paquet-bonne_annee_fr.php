<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/bonne_annee.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bonne_annee_description' => 'Affiche tous les ans, durant le mois de janvier, un message souhaitant une bonne année.
Ajouter <code>#INCLURE{fond=bonne_annee}</code> dans vos pages.',
	'bonne_annee_nom' => 'Bonne année',
	'bonne_annee_slogan' => 'Souhaiter la bonne année sur votre site'
);
